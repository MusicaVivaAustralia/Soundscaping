"use strict";

ui.initElement( "newtrack", function( el ) {
    function toggle( b ) {
        ui.newTrack();
    }

    return {
        click: toggle,
        toggle: toggle
    };
} );
