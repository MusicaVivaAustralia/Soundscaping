"use strict";

ui.initElement( "btnDeleteAll", function( el ) {
    return {
        click: function( ) {
            gs.composition.samples.forEach( function( s ) {
                gs.sample.select( s, true );
            } );
            gs.samples.selected.delete();
        }
    };
} );
