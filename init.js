"use strict";

( function() {

gs.wctx = new gswaContext();
gs.ctx = gs.wctx.ctx;
gs.analyserNode = gs.wctx.ctx.createAnalyser();
gs.analyserNode.fftSize = 256;
gs.analyserData = new Uint8Array( gs.analyserNode.frequencyBinCount ),
gs.composition = gs.wctx.createComposition();
gs.wctx.filters.pushBack( gs.analyserNode );

ui.resize();
ui.setFilesWidth( 200 );
ui.setTrackLinesLeft( 2 );
ui.setTrackNamesWidth( 125 );
ui.setGridZoom( 2.2, 0 );
ui.visual.on();
ui.btnMagnet.toggle( true );
ui.tracksBg.update();
ui.timelineLoop.toggle( false );

gs.history.reset();
gs.bpm( 120 );
gs.currentTime( 0 );
gs.compositions.init();
gs.composition.onended( gs.compositionStop );
ui.dom.btnFiles.click();
ui.dom.clockUnits.querySelector( ".s" ).click();
ui.dom.menu.querySelector( "[data-tool='paint']" ).click();

var tracksForHeight = Math.floor(window.innerHeight / 135);

for ( var i = 0; i < tracksForHeight; ++i ) {
	ui.newTrack();
}

ui.trackHeight = ui.tracks[ 0 ].elColNamesTrack.offsetHeight;

var urlParams = location.search.substring(1);
urlParams = JSON.parse('{"' + decodeURI(urlParams).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')

ui.getJSON(urlParams.url, function(status, results) {
    results.files.forEach(function(obj) {
        ui.getFileObject(obj.url, obj.name , function (fileObject) {
            var file = gs.file.create(fileObject)
            gs.file.load( file, function(){} );
        });
    });
});

} )();
