"use strict";

ui.addColour = function() {

    var trackColors = {};

    document.querySelectorAll("#filesList .item").forEach(function(trackElement) {
        var trackColor = window.getComputedStyle(trackElement, null).getPropertyValue( 'background-color' );
        var trackName  = trackElement.querySelector('span.name').innerHTML;
        trackColors[trackName] = trackColor;
    });

    document.querySelectorAll("#tracksLines .gridBlock.sample").forEach(function(trackElement) {
        var trackName = trackElement.querySelector('b.name').innerHTML;
        trackElement.style.background = trackColors[trackName];
    });
};
