"use strict";

ui.getFileBlob = function (url, cb) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url);
        xhr.responseType = "blob";
        xhr.addEventListener('load', function() {
            cb(xhr.response);
        });
        xhr.send();
};

ui.blobToFile = function (blob, name) {
        blob.lastModifiedDate = new Date();
        blob.name = name;
        return blob;
};

ui.getFileObject = function(filePathOrUrl, fileName,  cb) {
       ui.getFileBlob(filePathOrUrl, function (blob) {
          cb(ui.blobToFile(blob, fileName + ".wav" ));
       });
};
