"use strict";

gs.file.click = function( that ) {
	if ( that.isLoaded ) {
		gs.file.play( that );
	} else if ( !that.file ) {
		ui.gsuiPopup.open( "confirm", "Sample's data missing",
			"<code>" + that.name + "</code> is missing...<br/>" +
			"Do you want to browse your files to find it ?" )
		.then( function( b ) {
			if ( b ) {
				ui.filesInput.getFile( function( file ) {
					gs.file.joinFile( that, file );
				} );
			}
		} );
	} else if ( !that.isLoading ) {
		if (gs.files.length < 12) {
			gs.file.load( that, gs.file.play );
		} else {
			ui.gsuiPopup.open( "ok", "File limit reached", "Soundscaping currently has a 12 file limit. No more files can be loaded." )
		}
	}
};
