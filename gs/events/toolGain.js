"use strict";

( function() {

ui.tool.gain = {
    start: function() {
        ui.cursor( "grid", "grab" );
    },
    end: function() {
        ui.cursor( "grid", null );
    },
    mousedown: function( e ) {
        _smp = e.target.gsSample;
        if ( _smp ) {
            _offset = _smp.offset;
        }
    },
    mouseup: function() {
        _smp = null;
    },
    mousemove: function( e, secRel ) {
        if ( _smp ) {
            return gs.samples.selected.gain( _smp, secRel );
        }
    }
};


var _smp,
    _offset;

} )();
