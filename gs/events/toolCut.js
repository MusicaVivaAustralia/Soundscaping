"use strict";

( function() {

ui.tool.cut = {
	mousedown: function( e ) {
		_smp = e.target.gsSample;
        ui.addColour();
	},
	mouseup: function( e ) {
		if ( _smp ) {
			gs.samples.selected.cut( _smp, ui.getGridSec( e.pageX ) );
		}
		_smp = null;
        ui.addColour();
	}
};

var _smp;

} )();
