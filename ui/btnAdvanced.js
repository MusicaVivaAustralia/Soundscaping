"use strict";

ui.isAdvanced = false;

ui.initElement( "cb1", function( el ) {
    function toggle( b ) {
        if ( typeof b !== "boolean" ) {
            b = !ui.isAdvanced;
        }

        ui.isAdvanced = b;

        var advanced_elements = document.getElementsByClassName('advanced');

        for (var i = 0; i < advanced_elements.length; ++i) {
            advanced_elements[i].style.display = (ui.isAdvanced ? 'block' : 'none');
        }
    }

    return {
        click: toggle,
        toggle: toggle
    };
} );
