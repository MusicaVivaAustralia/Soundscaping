"use strict";

ui.initElement( "btnReturn", function( el ) {
    return {
        click: function() {
            gs.composition.currentTime( 0 );
            ui.currentTimeCursor.at( 0 );
            ui.clock.setTime( 0 );
        }
    };
} );
